##
# Flake nodejs projects
#
# @file
# @version 0.1
.PHONY: setup clean

setup:
	@mkdir -p _gen/vscode_eslint
	@wget "https://github.com/emacs-lsp/lsp-server-binaries/blob/master/dbaeumer.vscode-eslint-2.2.2.vsix?raw=true" -O _gen/vscode_eslint/server.zip
	@unzip -ud _gen/vscode_eslint _gen/vscode_eslint/server.zip
	@rm _gen/vscode_eslint/server.zip

clean:
	rm -rf _gen

# end
