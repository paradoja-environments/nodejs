{
  description = "Node dev environment";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = let inherit (nixpkgs.lib) optional optionals;

        in pkgs.mkShell {
          buildInputs = with pkgs; [
            nodejs
            nodePackages.eslint
            nixfmt
            yaml-language-server
          ];
          shellHook = "";
        };
      });
}
