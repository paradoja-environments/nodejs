;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((eval . (let ((eslint-path (executable-find "eslint"))
                       (npm-path (executable-find "npm"))
                       (node-path (executable-find "node"))
                       (project-path (file-name-directory
                                      (let ((d (dir-locals-find-file "./")))
                                        (if (stringp d) d (car d))))))
                   (setq flycheck-javascript-eslint-executable eslint-path
                         lsp-eslint-unzipped-path eslint-path
                         lsp-eslint-runtime npm-path
                         lsp-eslint-node node-path
                         lsp-eslint-server-command `(,node-path
                                                    ,(f-join project-path
                                                            "../environments/nodejs/_gen/vscode_eslint/extension/server/out/eslintServer.js")
                                                    "--stdio"))))))
 (web-mode . ((+format-on-save-enabled-modes . (not web-mode))))
 (org-mode . ((eval . (setq org-hugo-base-dir (projectile-project-root)))))
 (js2 . ((js2-basic-offset . 4))))
